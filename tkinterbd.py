import firebase_admin
from firebase_admin import credentials
from firebase_admin import db


from pyfirmata import Arduino, util
from tkinter import *
from PIL import Image
from PIL import ImageTk
import time
adc_data,cont=0,0
placa = Arduino ('COM6')
it = util.Iterator(placa)
it.start()
a_0 = placa.get_pin('a:0:i')
a_1 = placa.get_pin('a:1:i')
time.sleep(0.5)
ventana = Tk()
ventana.geometry('1280x800')
ventana.title("UI para sistemas de control")

# Fetch the service account key JSON file contents
cred = credentials.Certificate('C:/Users/Camilo/Desktop/db_tkinter/secreto/llave.json')
# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://bdtkinter.firebaseio.com/'
})


marco1 = Frame(ventana, bg="gray", highlightthickness=1, width=1280, height=800, bd= 5)
marco1.place(x = 0,y = 0)
b=Label(marco1,text="")
img = Image.open("C:/Users/Camilo/Downloads/logousa.png")
img = img.resize((150,150))
photoImg=  ImageTk.PhotoImage(img)
b.configure(image=photoImg)
b.place(x = 760,y = 20)


def medir():
    global cont
    ref = db.reference("adc")
    adc_data=a_0.read() 
    adc_data1=a_1.read() 
    ref.update({
                'adc1': {
                    'contador': cont ,
                     'valor': adc_data 
                },
                 'adc2': {
                    'contador': cont ,
                     'valor': adc_data1 
                }
         })                  
    adc_indicador['text']=str(adc_data)
    cont+=1
    print(cont)
    ventana.after(5000,medir)
    
Label(ventana, text="Valor ADC0 ").place(x=30, y=60)
adc_indicador=Label(ventana, text=str(adc_data), bg='cadet blue1', font=("Arial Bold", 14), fg="white")
adc_indicador.place(x=40, y=90)

medir()
    

ventana.mainloop()

    
    
